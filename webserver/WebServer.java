import java.io.*;
import java.net.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

import se.lth.cs.fakecamera.*; // To gain access to maximum image size

public class WebServer {

    public static void main(String[] args) {
	if (args.length!=2) {
	    System.out.println("Syntax: JPEGHTTPClient <address> <port>");
	    System.exit(1);
	}
	new GUI(args[0],Integer.parseInt(args[1]));
    }

}

class ImagePanel extends JPanel {
    ImageIcon icon;
	
    public ImagePanel() {
	super();
	icon = new ImageIcon();
	JLabel label = new JLabel(icon);
	add(label, BorderLayout.CENTER);
	this.setSize(200, 200);
    }
    	
    public void refresh(byte[] data) {
	Image theImage = getToolkit().createImage(data);
	getToolkit().prepareImage(theImage,-1,-1,null);
	icon.setImage(theImage);
	icon.paintIcon(this, this.getGraphics(), 5, 5);
    }
}
 
class ButtonHandler implements ActionListener {

    GUI gui;

    public ButtonHandler(GUI gui) {
	this.gui = gui;
    }

    public void actionPerformed(ActionEvent evt) {
	gui.refreshImage();
    }
}

class GUI extends JFrame {

    ImagePanel imagePanel;
    JButton button;
    boolean firstCall = true;
    String server;
    int port;
    byte [] jpeg = new byte[Axis211A.IMAGE_BUFFER_SIZE];
	boolean connect;
	Socket sock;
	InputStream is;
	OutputStream os;

    public GUI(String server,int port) {
	super();
	this.server = server;
	this.port = port;
	this.imagePanel = new ImagePanel();
	this.button = new JButton("Get image");
	this.button.addActionListener(new ButtonHandler(this));
	this.getContentPane().setLayout(new BorderLayout());
	this.getContentPane().add(imagePanel, BorderLayout.NORTH);
	this.getContentPane().add(button, BorderLayout.SOUTH);
	this.setLocationRelativeTo(null);
	this.pack();
	this.refreshImage();
	
    }

    public void refreshImage() {
        try {
            
            // Open a socket to the server, get the input/output streams
            this.sock = new Socket(server, port);
            this.is = sock.getInputStream();
            this.os = sock.getOutputStream();
		
		
			System.out.println("Ready to read size string..");
			String size = getLine(is); // read SIZE
			System.out.println("Read size string");
			
			int bufferSize = Integer.parseInt(size);
		
			this.jpeg = new byte[bufferSize];


            // Now load the JPEG image.
            int bytesRead  = 0;
            int bytesLeft  = bufferSize;
            int status;

            // We have to keep reading until -1 (meaning "end of file") is
            // returned. The socket (which the stream is connected to)
            // does not wait until all data is available; instead it
            // returns if nothing arrived for some (short) time.
             do {
                status = is.read(jpeg, bytesRead, bytesLeft);
                // The 'status' variable now holds the no. of bytes read,
                // or -1 if no more data is available
                if (status > 0) {
                    bytesRead += status;
                    bytesLeft -= status;
                }
            } while (status > 0);
			
			System.out.println("read the image, size: " + bytesRead);
			
			this.sock.close();

        }
        catch (IOException e) {
            System.out.println("Error when receiving image.");
            return;
        }
		
		imagePanel.refresh(jpeg);
		
		if (this.firstCall) {
		    this.pack();
		    this.setVisible(true);
		    this.firstCall = false;
		}
	}
    // -------------------------------------------------------- PRIVATE METHODS

    private static final byte[] CRLF      = { 13, 10 };

    /**
     * Read a line from InputStream 's', terminated by CRLF. The CRLF is
     * not included in the returned string.
     */
    private static String getLine(InputStream s) throws IOException {
        boolean done = false;
        String result = "";

        while(!done) {
            int ch = s.read();        // Read
            if (ch <= 0 || ch == 10) {
                // Something < 0 means end of data (closed socket)
                // ASCII 10 (line feed) means end of line
                done = true;
            }
            else if (ch >= ' ') {
                result += (char)ch;
            }
        }

        return result;
    }

    /**
     * Send a line on OutputStream 's', terminated by CRLF. The CRLF should not
     * be included in the string str.
     */
    private static void putLine(OutputStream s, String str) throws IOException {
        s.write(str.getBytes());
        s.write(CRLF);
    }

}