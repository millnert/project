

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

//import se.lth.cs.fakecamera.*; 


public class CMDReciever extends Thread {
	
	
	private static final byte[] CRLF      = { 13, 10 };
	private OutputStream os;
	private InputStream is;
	private String CMD;
	private int port;
	private ShareDataCam camShare;
	private ServerSocket serverSocket;
	private Socket cliSocket;
	
	
	public CMDReciever(int port, ShareDataCam sd){
		
		this.camShare = sd;
		this.port = port;
	}
	
	public void run() {
		 
	boolean terminate;
	
	try{
		this.serverSocket = new ServerSocket(port);
		this.cliSocket = serverSocket.accept();
		this.is = cliSocket.getInputStream();
		this.os = cliSocket.getOutputStream();
		
	}catch(IOException e){
		e.printStackTrace();
	}
	
	while (true) {
				try {
					
					CMD = getLine(is);
										
					if(CMD.equals("CONNECT")){
						camShare.connect(cliSocket, is, os);
					}else if (CMD.equals("GET") && (!camShare.getMotion() || camShare.getFidle())){	
						camShare.CMDSend();
						
					}else if (CMD.equals("DISCONNECT")) {
						camShare.disconnect();
						
					}else if(CMD.equals("FIDLE")){
						camShare.setFidle(true);
						camShare.setMotion(false);
					
					}else if (CMD.equals("NFIDLE")){
						camShare.setFidle(false);
					
					}else if(CMD.equals("QUIT")){
						camShare.quit();
					}
						
				}catch (Exception e){ e.printStackTrace();}

			}
	}

	
	
	
/////////////////PUTLINE - GETLINE//////////////////////
	private static void putLine(OutputStream s, String str) throws IOException {
        s.write(str.getBytes());
        s.write(CRLF);
    }
	
	
	
    private static String getLine(InputStream s) throws IOException {
        boolean done = false;
        String result = "";

        while(!done) {
            int ch = s.read();        // Read
            if (ch <= 0 || ch == 10) {
                // Something < 0 means end of data (closed socket)
                // ASCII 10 (line feed) means end of line
                done = true;
            }
            else if (ch >= ' ') {
                result += (char)ch;
            }
        }

        return result;
    }
}
	
	
	
	
