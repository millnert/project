
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;


public class WebReciever extends Thread{
	
	
	
	private static final byte[] CRLF      = { 13, 10 };
	private String CMD;
	private int port;
	private ShareDataCam camShare;
	private OutputStream os;
	private InputStream is;
	private ServerSocket serverSocket;
	private Socket cliSocket;
	
	public WebReciever(int port, ShareDataCam sd){
		this.camShare = sd;
		this.port = port;
		
	}
	
	public void run() {
		 
	boolean terminate;
	try{
		this.serverSocket = new ServerSocket(port);		
	}catch(IOException e){
		System.out.println(e);
	}
	
				
		while (true) {
				try {
				
					this.cliSocket = serverSocket.accept();
					this.os = cliSocket.getOutputStream();					
					
					camShare.webSend(os);
					
					
					this.os.flush();
					this.cliSocket.close();
						
				}catch (Exception e){ 
					e.printStackTrace();
				}

		}
	}

	
	
	
/////////////////PUTLINE - GETLINE//////////////////////
	private static void putLine(OutputStream s, String str) throws IOException {
        s.write(str.getBytes());
        s.write(CRLF);
    }
	
	
	
    private static String getLine(InputStream s) throws IOException {
        boolean done = false;
        String result = "";

        while(!done) {
            int ch = s.read();        // Read
            if (ch <= 0 || ch == 10) {
                // Something < 0 means end of data (closed socket)
                // ASCII 10 (line feed) means end of line
                done = true;
            }
            else if (ch >= ' ') {
                result += (char)ch;
            }
        }

        return result;
    }
}
	
	
	
	
	