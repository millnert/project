

import java.io.*;
import java.net.*;


public class ShareDataCam {

	private static final byte[] CRLF = {13, 10};
	public boolean active;
	public boolean fidle;
	public boolean motion;
	private byte[] newestImage;
	private int	newestImageSize;
	private OutputStream os;
	private InputStream is;
	private Socket cliSocket;
	

	
	public ShareDataCam(){
		newestImage = new byte[0];
		active = false;
	}

	
	public synchronized boolean getMotion(){
		return motion;
	}
	
	public synchronized void setMotion(boolean mot){
		this.motion = mot;
		notifyAll();
	}
	
	public synchronized void setFidle(boolean setTo){
		this.fidle = setTo;
		notifyAll();
	}
	
	public synchronized boolean getFidle(){
		return fidle;
	}
	
	public synchronized void connect(Socket cliSocket, InputStream is, OutputStream os){
		this.cliSocket = cliSocket;
		this.is = is;
		this.os = os;
		this.active = true;
		notifyAll();
	}
	
	public synchronized void disconnect(){
		this.active = false;
		notifyAll();
		this.CMDSend(); // send one last image
		
	}
	
	public synchronized void quit(){
		try{
			os.flush();
			cliSocket.close();
			System.exit(1);
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public synchronized void motionSend(boolean motion, byte[] image, int imageSizenbr){
		
		
		if(!this.motion && motion){	
			this.motion = true; 
			notifyAll();
		}
		
		if(this.motion && !fidle && active){ 
			if(this.motion){
				try {						
					putLine(os, new String("MOTION"));	
					String imageSize = String.valueOf(imageSizenbr);
					putLine(os,imageSize);
					os.write(image,0,imageSizenbr);
				} catch (IOException e) { 
					e.printStackTrace();
				}
			}
		
		}
		this.newestImage = image;
		this.newestImageSize = imageSizenbr;
		notifyAll();
	}
	
	public synchronized void CMDSend(){
		
			try{
				while(newestImageSize == 0) wait();
			}catch(Exception e){
				e.printStackTrace();
			}
			try{						
				putLine(os, new String("NOMOTION"));	
				String imageSize = String.valueOf(newestImageSize);
				putLine(os,imageSize);
				os.write(newestImage,0,newestImageSize);
			} catch (IOException e) { 
				e.printStackTrace();
			}
		
	}
	
	public synchronized void webSend(OutputStream cos){
		
			try{
				while(newestImageSize == 0) wait();
			}catch(Exception e){
				e.printStackTrace();
			}
			try{						
				String imageSize = String.valueOf(newestImageSize);
				putLine(cos,imageSize);
				cos.write(newestImage,0,newestImageSize);
			} catch (IOException e) { 
				e.printStackTrace();
			}
		
	}
	

	private static void putLine(OutputStream s, String str) throws IOException {
        s.write(str.getBytes());
        s.write(CRLF);
    }

	
}