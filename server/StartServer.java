



public class StartServer {
	
	public static void main(String[] args){
        if (args.length!=2) {
            System.out.println("Syntax: StartServer <ClientPort> <WebPort>");
            System.exit(1);
        }
		
		int cliPort = Integer.parseInt(args[0]);
		int webPort = Integer.parseInt(args[1]);
		

		
		
		ShareDataCam sd = new ShareDataCam();

		
		WebReciever webrec = new WebReciever(webPort, sd);
		
		CMDReciever cmdrec = new CMDReciever(cliPort, sd);
		
		CameraCommunicator cc = new CameraCommunicator(sd);
	
		webrec.start();
	
		cmdrec.start();
		
		cc.start();
		
	
	}
	
	
//	
//    public static void main(String[]args) {
//        JPEGHTTP_Server httpServer = new JPEGHTTP_Server(6077);
//        httpServer.start();
//    }
	
	
}
