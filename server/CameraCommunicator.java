

                 
import se.lth.cs.fakecamera.*; 
//import se.lth.cs.cameraproxy.*; 
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;


//Sends connectRequest to AxisCamera and recieves images from it.
public class CameraCommunicator extends Thread{
	public static final int IMAGE_BUFFER_SIZE = 128*1024;

	private static final byte[] CRLF      = { 13, 10 };
	private byte[] image;
	private ShareDataCam shareCam;
	private OutputStream os;
	private InputStream is;
	private Socket socket;
	private Axis211A camera;
	private MotionDetector motionDetector;
//	private static final int imageBufferSize = Axis211A.IMAGE_BUFFER_SIZE;
	private int imageSizenbr;

	//needs communication with ShareDataCam.
	
	public CameraCommunicator(ShareDataCam shareCam){
		
		this.shareCam = shareCam;
		this.camera = new Axis211A(); //later with (String host, int port)
		this.motionDetector = new MotionDetector();
		this.image = new byte[IMAGE_BUFFER_SIZE];
		 
		
	}
	
	
	
	
	public void run() {
		
		while(true){
						
			imageSizenbr = camera.getJPEG(image, 0); //writes next image to "image" with 0 offset.
			boolean motion = motionDetector.detect();
			shareCam.motionSend(motion, image, imageSizenbr);
			try{
				sleep(30);
				
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	// private methods 
	
	private static void putLine(OutputStream s, String str) throws IOException {
        s.write(str.getBytes());
        s.write(CRLF);
    }
	
}












