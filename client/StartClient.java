

import java.io.*;
import java.net.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

import se.lth.cs.fakecamera.*; 

public class StartClient{


	public static void main(String[] args){
		
		SharedData sd = new SharedData();
		
		GetterThread gt1 = new GetterThread(1, sd);
		GetterThread gt2 = new GetterThread(2, sd);
		
		GUI gui = new GUI(sd);
		
		UpdateThread ut1 = new UpdateThread(1, sd, gui);
		UpdateThread ut2 = new UpdateThread(2, sd, gui);
		
		WritingThread wt = new WritingThread(sd);
		
		
		sd.setGUI(gui);
		
		gui.initializeGUI();
		gt1.start();
		gt2.start();
		ut1.start();
		ut2.start();
		wt.start();
		
		
	}



}