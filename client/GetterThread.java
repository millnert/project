

import java.io.*;
import java.net.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

import se.lth.cs.fakecamera.*; 


public class GetterThread extends Thread{
	
	// Attributes
	private Socket socket;
	private InputStream is;
	private int port;
	
	private int threadNumber; // to keep track of what this is (matters in SharedData)
	
	private SharedData sd;
	
	private boolean weShouldRun;
	
	public static final int IMAGE_BUFFER_SIZE = 128*1024;
	
	
	
	// Constructor
	public GetterThread(int threadNumber, SharedData sd){
		this.threadNumber = threadNumber;
		this.sd = sd;
		this.weShouldRun = true;
	}
	
	
	
	// Run Method
	public void run(){
		
		while(weShouldRun){
			try{
				this.socket = sd.getSocket(threadNumber);
				this.port = socket.getPort();
				this.is = socket.getInputStream();
				
				String motionStr = getLine(is); // read MOTION/NMOTION
						
				boolean motion = motionStr.equals("MOTION");
				
				String size = getLine(is); // read SIZE
				
				int bufferSize = Integer.parseInt(size);
			
				byte[] jpeg = new byte[bufferSize];


	            // Now load the JPEG image.
	            int bytesRead  = 0;
	            int bytesLeft  = bufferSize;
	            int status;

	            // We have to keep reading until -1 (meaning "end of file") is
	            // returned. The socket (which the stream is connected to)
	            // does not wait until all data is available; instead it
	            // returns if nothing arrived for some (short) time.
	             do {
 	                status = is.read(jpeg, bytesRead, bytesLeft);
 	                // The 'status' variable now holds the no. of bytes read,
 	                // or -1 if no more data is available
 	                if (status > 0) {
 	                    bytesRead += status;
 	                    bytesLeft -= status;
 	                }
 	            } while (status > 0);

									
				sd.newImage(jpeg, port, motion, threadNumber);
				
					
			}catch(IOException e){
				e.printStackTrace();
			}
		}
		
	}
	
	// -----------------------------------------------------------------------------
	// ------------------------- Private Methods & Classes -------------------------
	// -----------------------------------------------------------------------------
	
    private static String getLine(InputStream s) throws IOException {
        boolean done = false;
        String result = "";

        while(!done) {
            int ch = s.read();        // Read
            if (ch <= 0 || ch == 10) {
                // Something < 0 means end of data (closed socket)
                // ASCII 10 (line feed) means end of line
                done = true;
            }
            else if (ch >= ' ') {
                result += (char)ch;
            }
        }

        return result;
    }
		
	
	
}