

import java.io.*;
import java.net.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

import se.lth.cs.fakecamera.*; 

// Monitor for the Client Object
public class SharedData {
	
	// Put the attributes here
	// -----------------------------------------------------------------------------
	private static final int IDLE = 0, MOVIE = 1, AUTO = 1, MANUAL = 0, SYNC = 1, ASYNC = 0;
	private static final int NBRCAMERAS = 2; // two different cameras for now...
	private static final double SYNCHTHRESHOLD = 200; // in millis
	private static final int SAMPLES = 5; // number of past images to calc the avg delay from
	private static final long DELTA = 500; // time from capture to playback (when in sync-mode)
	private static final int BUFFERSIZE = 5000; // size of the buffer
	private static final int MAXCONNECTIONS = 20; // maxium possible cameras to connect to
	
	private boolean idle; 
	private boolean sync;
	private boolean forcedMode; // If the current mode is forced
	private boolean forcedSyncMode; // If the operator forces sync mode
	
	private String msg[]; // msg to servers. DISCONNECT, GET, FIDLE, NFIDLE, QUIT, CONNECT 
	private boolean newMsg;

	// Which camera triggered the movie mode
	
	// Hosts connections : just the ports
	private Socket[] sockets; // vector to store ports
	private boolean[] connections; // boolean to determine whether the sockets above are connected or not
	
	// JPEG images  - contains timestamp, port, jpeg & arrival time to client
	private Image[] image0;
	private int putImage0;
	private int getImage0;
	private Image image0LastSent;
	private long[] delay0;
	private int putDelay0;
	private int getDelay0;
	private long delaySum0;
	
	private Image[] image1;
	private Image image1LastSent;
	private int putImage1;
	private int getImage1;
	private long[] delay1;
	private int putDelay1;
	private int getDelay1;
	private long delaySum1;
	
	private Socket[] previousConnections;
	
	private GUI gui;
	
	// ------------------------------------------------------------------------
	// --------------------- Public Methods -----------------------------------
	// ------------------------------------------------------------------------
	
	
	// Constructor for the monitor
	public SharedData(){
		this.sockets = new Socket[NBRCAMERAS];
		
		this.image0 = new Image[BUFFERSIZE];
		this.image1 = new Image[BUFFERSIZE];

		this.delay0 = new long[SAMPLES];
		this.delay1 = new long[SAMPLES];	
			
		this.connections = new boolean[NBRCAMERAS];
		
		this.image0LastSent = new Image();
		this.image1LastSent = new Image();
		
		this.msg = new String[2];
		this.msg[0] = new String("GET");
		this.msg[1] = new String("GET");
		
		this.idle = true;
		this.forcedMode = false;
		this.forcedSyncMode = false;
		
		this.previousConnections = new Socket[MAXCONNECTIONS];
		
	}
	
	// Method to set the GUI
	public synchronized void setGUI(GUI gui){
		this.gui = gui;
	}

	public synchronized void refreshGUIButtons(){
		gui.controlIdleMode(idle);
		gui.controlSynMode(sync);
		gui.controlAutoMode(!forcedMode);
	}

	// Method to refresh the GUI
	// called by the updateGUI-threads (2)
	public synchronized void refreshGUIImages(int threadNumber){
		// Call the gui and send what mode we're in!
		try{
				switch(threadNumber){
				case 1:{
					// wait for an image
					while(getImage0 == putImage0 || image0[getImage0] == null ) wait();
					
					Image image = image0[getImage0];
		
					// wait till display time if we're in sync-mode
					while(sync && (image.p > System.currentTimeMillis()) ) wait(image.p - System.currentTimeMillis());
		
					gui.refreshImage1(image.jpeg, image.d);
		
					if(++getImage0 >= image0.length) getImage0 = 0;
				
					break;
				}
			
				case 2:{
					// wait for an image
					while(getImage1 == putImage1 || image1[getImage1] == null) wait();
		
					Image image = image1[getImage1];
		
					// wait till display time if we're in sync-mode
					while(sync && (image.p > System.currentTimeMillis()) ) wait(image.p - System.currentTimeMillis());
		
					gui.refreshImage2(image.jpeg, image.d);
		
					if(++getImage1 >= image1.length) getImage1 = 0;
					break;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		

	}
	
	// Method to get the messege to send to the servers
	// Called  by the WritingThread
	public synchronized void sendMessageToServers(){
		
		try{
			long t = System.currentTimeMillis() + 5*1000;
	
			while(idle && !newMsg && (t - System.currentTimeMillis()) > 0) wait(t - System.currentTimeMillis());
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try{
			if(connections[0]){
				OutputStream os0 = sockets[0].getOutputStream();
				putLine(os0, msg[0]);
			}
			if(connections[1]){
				OutputStream os1 = sockets[1].getOutputStream();
				putLine(os1, msg[1]);
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		
		// set their connection to false (so that we don't send more msgs)
		if(msg[0].equals("DISCONNECT")) connections[0] = false; 

		// set their connection to false (so that we don't send more msgs)
		if(msg[1].equals("DISCONNECT")) connections[1] = false;
		
		if(!msg[0].equals("GET")) {
			this.msg[0] = new String("GET");
			this.newMsg = false;
		}
		if(!msg[1].equals("GET")) {
			this.msg[1] = new String("GET");
			this.newMsg = false;
		}
		notifyAll();		
	}
	
	// Method to send a new image. Called from the thread that reads the sockets.
	public synchronized void newImage(byte[] jpeg, int port, boolean motion, int threadNumber){
		Image image = new Image();
		image.jpeg = jpeg;
		image.port = port;
		image.c = getTimeStamp(jpeg); // time for capture of the image
		image.a = System.currentTimeMillis(); // time for arrival at the client of the image
		image.d = image.a - image.c;	// delay (time from capture to arrival at the client )
		image.p = image.c + DELTA; // time for playback if we're in sync-mode
		
		// Store the image and the delay in the correct buffer
		switch(threadNumber){
			case 1:{
				image0[putImage0] = image;
				delay0[putDelay0] = image.d;
				delaySum0 = sum(delay0);
				if(++putImage0 >= image0.length) putImage0 = 0;
				if(++putDelay0 >= delay0.length) putDelay0 = 0;
				break;
			}
			case 2:{
				image1[putImage1] = image;
				delay1[putDelay1] = image.d;
				if(++putImage1 >= image1.length) putImage1 = 0;
				if(++putDelay1 >= delay1.length) putDelay1 = 0;
				break;
			}
		}			
		
		
		// check if we should enter or exit sync-mode
		// if the average delay of the two streams from the 5 last images
		// differ with less than 0.2s we're in sync-mode
		if(Math.abs(delaySum0 - delaySum1) < SYNCHTHRESHOLD*SAMPLES && !forcedSyncMode){
			this.sync = true;
		}else if(!forcedSyncMode){ 
			this.sync = false;
		}
		
		notifyAll();
		
		// Check if we should switch from idle to movie
		if(idle && !forcedMode && motion) idle = false;
		
		notifyAll();
	}
	
	// Method to put the put the new servers
	public synchronized void setSocket(String host, int port, int side){
		try{
			if(side == 0){
				
				int check = checkForConnections(port, side);
				
				if(check == 0) { // null means there was no connection to this port
					this.sockets[0] =  new Socket(host, port);// open a new socket at the new connection..
					addToPreviousConnections(sockets[0]); // add this socket to previous connections..
				}
				this.msg[0] = new String("CONNECT");
				this.newMsg = true;
				this.connections[0] = true;
				notifyAll();
					
			}else if(side == 1){
				
				int check = checkForConnections(port, side);
				
				if(check == 0) {
					this.sockets[1] =  new Socket(host, port);// open a new socket at the new connection..
					addToPreviousConnections(sockets[1]); // add this socket to previous connections..
				}
				this.msg[1] = new String("CONNECT");
				this.newMsg = true;
				this.connections[1] = true;
				notifyAll();
				
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	// Method to get a socket. Called from the threads that read from the sockets.
	// 1 = first reading thread
	// 2 = second reading thread
	public synchronized Socket getSocket(int threadNumber){
		try{
			switch(threadNumber){
				case 1:{
					while(!connections[0]) wait(); // wait till a socket connection has been established
					return sockets[0];
				}
				case 2:{
					while(!connections[1]) wait();
					return sockets[1];
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	// Method called by the GUI action listeners
	public synchronized void syncMode(int mode){
		this.forcedSyncMode = true;
		this.sync = (mode == SYNC);
		notifyAll();
	}
	
	// Method called by the GUI action listeners
	public synchronized void movieMode(int mode){
		if((!forcedMode && (mode == IDLE) || (forcedMode && !idle))){ // entering forced idle mode for first time
			this.msg[0] = new String("FIDLE");
			this.msg[1] = new String("FIDLE");
			this.newMsg = true;	
			
			this.getImage0 = putImage0;
			this.getImage1 = putImage1;

		}
		this.forcedMode = true;
		this.idle = (mode == 0);
		notifyAll();
	}
	
	// Method called by the GUI action listeners
	public synchronized void autoMode(int mode){
		
		if(forcedMode && (mode == AUTO)){
			this.msg[0] = new String("NFIDLE"); // exiting forced idle mode (movie mode too for that matter, but that doesn't matter)
			this.msg[1] = new String("NFIDLE"); // exiting forced idle mode (movie mode too for that matter, but that doesn't matter)

			this.newMsg = true;
		}
		this.forcedMode = (mode == MANUAL);
		notifyAll();
	}
	
	// Methos to quit, called by GUI
	public synchronized void quit(){
		this.msg[0] = new String("QUIT");
		this.msg[1] = new String("QUIT");
		this.newMsg = true;
		sendMessageToServers();
		System.exit(1); // we just want to quit...
		
	}
	
	
	public synchronized void disconnect(int side){
		if(side == 0){ // disconnect from socket 0
			this.msg[0] = new String("DISCONNECT");
			this.newMsg = true;
			notifyAll();
		}else if(side == 1){ // disconnect form socket 1
			this.msg[1] = new String("DISCONNECT");
			this.newMsg = true;
			notifyAll();
		}
	}
	
	
	// -----------------------------------------------------------------------------
	// ------------------------- Private Methods & Classes -------------------------
	// -----------------------------------------------------------------------------
	
	// Search and check if we already established a connection to this camrea
	// returns the socket if we have, otherwise returns null.
	private int checkForConnections(int port, int side){
		for(int i = 0; i < MAXCONNECTIONS; i++){
			if(previousConnections[i] != null){
				if(previousConnections[i].getPort() == port) {
					if(side == 0){
						sockets[0] = previousConnections[i];
					}else if(side == 1){
						sockets[1] = previousConnections[i];
					}
					return 1; 
				}
					
			}
		}
		
		return 0;
	}
	
	private void addToPreviousConnections(Socket socket){
		for(int i = 0; i < MAXCONNECTIONS; i++){
			if(previousConnections[i] == null){
				previousConnections[i] = socket;
				return;
			}
		}
		
	}
	
	// Simple class for storing the timestamp and jpeg-vectors
	private class Image{
		// Attributes
		byte[] jpeg; // JPEG
		int port; // port that sent the image
		long c; // capture time at the server
		long a; // arival time at the client
		long d; // delay between capture time and arrival time
		long p; // playback time (if in sync-mode)
		
	}
	
	// calculate the sum of a vector
	private long sum(long[] v){
		long sum = 0;
		for(int i = 0; i<v.length; i++){
			sum += v[i];
		}
		return sum;
	}
	
	// method to get the timestamp from the jpeg
	private long getTimeStamp(byte[] jpeg){
		return ((jpeg[25] & 255L) << 24 | (jpeg[26] & 255L) << 16 | 
			(jpeg[27] & 255L) << 8 | jpeg[28] & 255L) * 1000 + (jpeg[29] & 255L) * 10;
		
	}
	
	
    private static final byte[] CRLF      = { 13, 10 };


    /**
     * Send a line on OutputStream 's', terminated by CRLF. The CRLF should not
     * be included in the string str.
     */
    private synchronized void putLine(OutputStream s, String str) throws IOException {
        s.write(str.getBytes());
        s.write(CRLF);
    }
	
	
	
	
	
	
	
	
	
}