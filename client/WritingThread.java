

import java.io.*;
import java.net.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

import se.lth.cs.fakecamera.*; 

public class WritingThread extends Thread{
	
	
	// Attributes
	private Socket[] sockets;
	private OutputStream[] os;

	private SharedData sd;
	
	private boolean weShouldRun;
	
	
	// Constructor
	public WritingThread(SharedData sd){
		this.sd = sd;
		this.sockets = new Socket[2];
		this.os = new OutputStream[2];
		this.weShouldRun = true;

	}
	
	
	
	// Run Method
	public void run(){
		
		while(weShouldRun){
			try{
			
				sd.sendMessageToServers();
				
				sleep(30); // Wait 40 ms, so we don't send get's all the time! (max fps @ camera = 25fps)									
			
			}catch(Exception e){
				System.out.println("Error when sending message:" + e);
			}
		}
		
	}
	
	// -----------------------------------------------------------------------------
	// ------------------------- Private Methods & Classes -------------------------
	// -----------------------------------------------------------------------------
	
    private static final byte[] CRLF      = { 13, 10 };

    /**
     * Read a line from InputStream 's', terminated by CRLF. The CRLF is
     * not included in the returned string.
     */
    private static String getLine(InputStream s) throws IOException {
        boolean done = false;
        String result = "";

        while(!done) {
            int ch = s.read();        // Read
            if (ch <= 0 || ch == 10) {
                // Something < 0 means end of data (closed socket)
                // ASCII 10 (line feed) means end of line
                done = true;
            }
            else if (ch >= ' ') {
                result += (char)ch;
            }
        }

        return result;
    }

    /**
     * Send a line on OutputStream 's', terminated by CRLF. The CRLF should not
     * be included in the string str.
     */
    private static void putLine(OutputStream s, String str) throws IOException {
        s.write(str.getBytes());
        s.write(CRLF);
    }
		
	
	
}