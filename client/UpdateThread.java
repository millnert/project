

import java.io.*;
import java.net.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

import se.lth.cs.fakecamera.*; 

public class UpdateThread extends Thread{
	
	// Attributes
	private SharedData sd;
	private int threadNumber;
	private boolean weShouldRun;
	private GUI gui;


	public UpdateThread(int threadNumber, SharedData sd, GUI gui){
		this.sd = sd;
		this.threadNumber = threadNumber;
		this.weShouldRun = true;
		this.gui = gui;
	}
	
	public void run(){
		while(weShouldRun){
			
			sd.refreshGUIButtons();
			gui.updateGUI();
			sd.refreshGUIImages(threadNumber);
						
		}
		
	}
	
}

