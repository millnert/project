

 import java.io.*;
 import java.net.*;
 import javax.swing.*;
 import java.awt.*;
 import java.awt.event.*;
 import java.awt.image.*;
 import se.lth.control.*;
 import se.lth.control.plot.*;
 import javax.swing.Box;
 import se.lth.cs.fakecamera.*; 


public class GUI extends JFrame {
	
	public static final int IDLE = 0, MOVIE = 1, ASYNCHRONIZED = 0, SYNCHRONIZED = 1, MANUAL = 0, AUTO = 1;
	private ImagePanel imagePanel1, imagePanel2;
	private JButton button;
	private boolean firstCall1 = true;
	private boolean firstCall2 = true;
	private boolean first = true;
	private byte [] jpeg = new byte[Axis211A.IMAGE_BUFFER_SIZE];
	private int port1,port2;
	private String server1,server2;
	private SharedData data;
	
	// fält
	private JTextField serverfield2 = new JTextField(8);
	private JTextField serverfield1 = new JTextField(8);
	private	IntegerField portfield1 = new IntegerField(5);
	private	IntegerField portfield2 = new IntegerField(5);
	private JPanel cameraIntFieldPanel1 = new JPanel();
	private JPanel cameraIntFieldPanel2 = new JPanel();
	private	JPanel cameraStringFieldPanel1 = new JPanel();
	private	JPanel cameraStringFieldPanel2 = new JPanel();
	private JPanel cameraPanel1 = new JPanel();
	private JPanel cameraPanel2 = new JPanel();
	private JPanel cameraPanel11 = new JPanel();
	private JPanel cameraPanel22 = new JPanel();
	private JPanel leftView = new JPanel();
	private JPanel rightView = new JPanel();
	
	// knappar
	private	JButton cameraConnectButton1 = new JButton("Connect");
	private	JButton cameraConnectButton2 = new JButton("Connect");
	private JButton cameraDisconnectButton1 = new JButton("Disconnect");
	private JButton cameraDisconnectButton2 = new JButton("Disconnect");
	private JRadioButton idleModeButton = new JRadioButton("IDLE");
	private JRadioButton movieModeButton = new JRadioButton("MOVIE");
	private JButton quitButton = new JButton("QUIT");
	private JRadioButton asynchronizedModeButton = new  JRadioButton("Asynchronized");
	private JRadioButton synchronizedModeButton = new JRadioButton("Synchronized");
	private JRadioButton autoModeButton = new JRadioButton("Auto");
	private JRadioButton manModeButton = new JRadioButton("Manual");
	private JLabel delayText1;
	private JLabel delayText2;
	
	// Data attributes
	private long delay1;
	private long delay2;
	private byte[] jpeg1;
	private byte[] jpeg2;
	private boolean controlAutoModeBoolean;
	private boolean controlIdleModeBoolean;
	private boolean controlSynModeBoolean;
	
	
	public GUI(SharedData data){
		super();
		this.data=data;
		this.port1=0;
		this.port2=0;
		this.server1=new String("in");
		this.server2=new String("in");
		this.delay1 = 0;
		this.delay2 = 0;
	}
	
	public void initializeGUI(){
		
	
		
		// Creates panels
		imagePanel1 = new ImagePanel();
		imagePanel2 = new ImagePanel();
		BoxPanel buttonPanel = new BoxPanel(BoxPanel.VERTICAL);

		// Group the radio buttons.
		ButtonGroup group1 = new ButtonGroup();
		ButtonGroup group2 = new ButtonGroup();
		ButtonGroup group3 = new ButtonGroup();
		
		group1.add(idleModeButton);
		group1.add(movieModeButton);
		group2.add(asynchronizedModeButton);
		group2.add(synchronizedModeButton);
		group3.add(autoModeButton);
		group3.add(manModeButton);
		
		// Button action listeners.
		manModeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){

				data.autoMode(MANUAL); // going into auto
			}
		});
		
		autoModeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				data.autoMode(AUTO);

			}
		});
		synchronizedModeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				data.syncMode(SYNCHRONIZED);
			}
		});
		asynchronizedModeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				data.syncMode(ASYNCHRONIZED);
			}
		});
		
		idleModeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				data.movieMode(IDLE);
			}
		});
		movieModeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				data.movieMode(MOVIE);
			}
		});

		
		quitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				data.quit();
			}
		});
		
		
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				data.quit();
				//System.exit(0);
			}
		});
		
		// Create camerapanel for the parameter which camera is to operate, 
		JPanel cameraPanel = new JPanel();
		
		// camera 1
		JPanel cameraStringLabelPanel1 = new JPanel();
		cameraStringLabelPanel1.setLayout(new GridLayout(0,1));
		cameraStringLabelPanel1.add(new JLabel("Server:"));
		cameraStringFieldPanel1.setLayout(new GridLayout(0,1));

		cameraStringFieldPanel1.add(serverfield1);
		
		JPanel cameraIntLabelPanel1 = new JPanel();
		cameraIntLabelPanel1.setLayout(new GridLayout(0,1));
		cameraIntLabelPanel1.add(new JLabel("Port:"));
		cameraIntFieldPanel1.setLayout(new GridLayout(0,1));
		cameraIntFieldPanel1.add(portfield1);
		
		// camera 2
		JPanel cameraStringLabelPanel2 = new JPanel();
		cameraStringLabelPanel2.setLayout(new GridLayout(0,1));
		cameraStringLabelPanel2.add(new JLabel("Server:"));
		cameraStringFieldPanel2.setLayout(new GridLayout(0,1));
		cameraStringFieldPanel2.add(serverfield2);
		
		JPanel cameraIntLabelPanel2 = new JPanel();
		cameraIntLabelPanel2.setLayout(new GridLayout(0,1));
		cameraIntLabelPanel2.add(new JLabel("Port:"));
		cameraIntFieldPanel2.setLayout(new GridLayout(0,1));
		cameraIntFieldPanel2.add(portfield2);
		
		
		
		
		//glue process camera
		

		
		cameraPanel11.add(cameraIntLabelPanel1);
		cameraPanel11.add(Box.createHorizontalGlue());
		cameraPanel11.add(cameraIntFieldPanel1);
		cameraPanel11.add(Box.createHorizontalGlue());
		cameraPanel11.add(cameraStringLabelPanel1);
		cameraPanel11.add(Box.createHorizontalGlue());
		cameraPanel11.add(cameraStringFieldPanel1);
		cameraPanel11.add(cameraConnectButton1);
		cameraPanel11.add(cameraDisconnectButton1);

		
		cameraPanel22.add(cameraIntLabelPanel2);
		cameraPanel22.add(Box.createHorizontalGlue());
		cameraPanel22.add(cameraIntFieldPanel2);
		cameraPanel22.add(Box.createHorizontalGlue());
		cameraPanel22.add(cameraStringLabelPanel2);
		cameraPanel22.add(Box.createHorizontalGlue());
		cameraPanel22.add(cameraStringFieldPanel2);
		cameraPanel22.add(cameraConnectButton2);
		cameraPanel22.add(cameraDisconnectButton2);
		
		
			
			
	
		// actionlistener
		portfield1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				port1 = (int) portfield1.getValue();
				cameraConnectButton1.setEnabled(true);
			}
		});
		serverfield1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				server1 = serverfield1.getText();
				cameraConnectButton1.setEnabled(true);
			}
		});
		portfield2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				port2 = (int) portfield2.getValue();
				cameraConnectButton2.setEnabled(true);
			}
		});
		serverfield2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				server2 = serverfield2.getText();
				cameraConnectButton2.setEnabled(true);
			}
		});
		
		
		
		//Connect Button
		cameraConnectButton1.setEnabled(false);
		cameraConnectButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("@GUI: Setting socket1, host: " + server1 + ", port: " + port1);
				data.setSocket(server1,port1,0);
				System.out.println("@GUI: Socket1 set!");
				cameraConnectButton1.setEnabled(false);
				cameraDisconnectButton1.setEnabled(true);
				
			}
		});
		
		cameraDisconnectButton1.setEnabled(false);
		cameraDisconnectButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				data.disconnect(0); // left side
				cameraDisconnectButton1.setEnabled(false);
			}
		});
		
		
		
		cameraConnectButton2.setEnabled(false);
		cameraConnectButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("@GUI: Setting socket2, host: " + server2 + ", port: " + port2);
				data.setSocket(server2,port2,1);
				System.out.println("@GUI: Socket2 set!");
				cameraConnectButton2.setEnabled(false);
				cameraDisconnectButton2.setEnabled(true);
				
			}
		});
		
		cameraDisconnectButton2.setEnabled(false);
		cameraDisconnectButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				data.disconnect(1); // left side
				cameraDisconnectButton2.setEnabled(false);
				
			}
		});
		
		
		// Set initial value
		portfield1.setValue(0);
		portfield2.setValue(0);
		serverfield1.setText("localhost");
		serverfield2.setText("localhost");
		
		idleModeButton.setSelected(true);
		asynchronizedModeButton.setSelected(true);
		manModeButton.setSelected(true);

		


		
		// Add buttons to button panel.
		buttonPanel.add(synchronizedModeButton);
		buttonPanel.add(asynchronizedModeButton);
		buttonPanel.add(idleModeButton);
		buttonPanel.add(movieModeButton);
		buttonPanel.add(manModeButton);
		buttonPanel.add(autoModeButton);
		buttonPanel.add(quitButton);
		
		
		

		
		//ihopsättning av cameraview
		cameraPanel1.setLayout(new BorderLayout());
		cameraPanel1.add(imagePanel1, BorderLayout.NORTH);
		cameraPanel1.add(Box.createRigidArea(new Dimension(0,5)));
		delayText1 = new JLabel("Delay: " + delay1);
		cameraPanel1.add(delayText1, BorderLayout.SOUTH);
		
		
		cameraPanel2.setLayout(new BorderLayout());
		cameraPanel2.add(imagePanel2, BorderLayout.NORTH);
		cameraPanel2.add(Box.createRigidArea(new Dimension(0,5)));
		delayText2 = new JLabel("Delay: " + delay2);
		cameraPanel2.add(delayText2, BorderLayout.SOUTH);
		
		
		leftView.setLayout(new BorderLayout());
		leftView.add(cameraPanel11, BorderLayout.NORTH);
		leftView.add(cameraPanel1, BorderLayout.SOUTH);
		leftView.setBorder(BorderFactory.createTitledBorder(
                       BorderFactory.createEtchedBorder(), "Left Camera"));
		
		
		rightView.setLayout(new BorderLayout());
		rightView.add(cameraPanel22, BorderLayout.NORTH);
		rightView.add(cameraPanel2, BorderLayout.SOUTH);
		rightView.setBorder(BorderFactory.createTitledBorder(
                       BorderFactory.createEtchedBorder(), "Right Camera"));
		
		// Add 
		this.getContentPane().setLayout(new BorderLayout());
		this.add(leftView, BorderLayout.WEST);
		this.add(rightView, BorderLayout.CENTER);
		this.getContentPane().add(buttonPanel, BorderLayout.EAST);
	

		// Pack the components of the window
		this.pack();
		
		// Position the main window at the screen center.
		Dimension sd = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension fd = this.getSize();
		this.setLocation((sd.width-fd.width)/2, (sd.height-fd.height)/2);
		
		//borde göra en refresh av bilderna hur funkar det att visa utan att ha en bild??

		// -------------------------------------------------
		// -------------------------------------------------
		// We have to add an image before we set the window
		// to visible... get a blank or black image???
		// -------------------------------------------------
		// VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV 
		
        Axis211A myCamera = new Axis211A();

        byte[] jpeg = new byte[Axis211A.IMAGE_BUFFER_SIZE];
		
        //int len = myCamera.getJPEG(jpeg,0);
		
		jpeg1 = jpeg2 = jpeg;
		/*
		imagePanel1.refresh(jpeg);
		imagePanel2.refresh(jpeg);
		*/
			
		this.pack();
		this.setVisible(true);
		
		// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		// --------------------------------------------------
		// -------------------- Testing ---------------------
		// --------------------------------------------------
		// --------------------------------------------------
		
		
	}
	
	
    public void refreshImage1(byte [] jpeg1,long delay1) {
		this.delay1=delay1;
		this.jpeg1=jpeg1;
	}


    public void refreshImage2(byte [] jpeg2,long delay2) {
		this.delay2=delay2;
		this.jpeg2=jpeg2;
	}
	
	public void controlIdleMode(boolean idle){
		this.controlIdleModeBoolean = idle;
	}
	
	public void controlSynMode(boolean syn){
		this.controlSynModeBoolean = syn;
	}
	
	public void controlAutoMode(boolean auto){
		this.controlAutoModeBoolean = auto;
	}
	
	public void update(){
		
		this.delayText1.setText("Delay: " + this.delay1);
		this.delayText1.update( this.delayText1.getGraphics());
		
		this.imagePanel1.refresh(this.jpeg1);
		
		this.delayText2.setText("Delay: " + this.delay2);
		this.delayText2.update( this.delayText2.getGraphics());
		
		this.imagePanel2.refresh(this.jpeg2);
		
		if(this.controlIdleModeBoolean){
			idleModeButton.setSelected(true);
		}else{
			movieModeButton.setSelected(true);
		}
		
		if(this.controlSynModeBoolean){
			synchronizedModeButton.setSelected(true);
		}else{
			asynchronizedModeButton.setSelected(true);
		}
		
		if(this.controlAutoModeBoolean){
			autoModeButton.setSelected(true);
		}else{
			manModeButton.setSelected(true);		
		}
	}

	public void updateGUI(){
		Runnable updateNow = new Runnable(){
			public void run(){
				update();
			}
		};
		try{
			SwingUtilities.invokeAndWait(updateNow);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}

	


